CONTENTS OF THIS FILE
---------------------
  
* Introduction
* Installation
* Configuration
* FAQ


INTRODUCTION
------------

Drupal to SugarCRM Lead module submit your custom form data to your SugarCRM Lead module. This will provided feature to Connect your SugarCRM.

Drupal to SugarCRM Lead module is simple yet advance, easy & one time setup solution for your business needs. Module will dinamically generate a Lead-form on a Block based on your choices using fields which are mapped to your SugarCRM Lead module. You can change the Order & Label of the field any time you want. Now convert your website traffic/visitors into business Leads. It was never that easy before.

   
INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

* After enabling module. Clear site cache (Recommended).
* Go to Configuration -> Content authoring -> SugarCRM module configuration,
  Enter your sugarCRM credentials and test connection. If connection establish successfully then save settings
  Then hit syncronize fields buttons
* Now go to Configuration -> Content authoring -> SugarCRM module field mapping,
  You can choose which fields you want to display on front end form. Update their label, display order etc.
* Now go to Structure -> Blocks & Drag and drop 'SugarCRM Lead Form' to whichever area you want.


FAQ
---

Q : Which user credentials I need to set?

A : You must have to set Administrator level user/pass to submit form data into SugarCRM Lead module. Once you have successfully tested your credentials, Click on SAVE button to save your User/Password into drupal database.
 