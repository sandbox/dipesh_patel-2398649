/** 
 * Page reload javascript function
 */
(function($, Drupal) {
    $.fn.reloadPage = function() {
        location.reload(true);
    };
})(jQuery, Drupal);